import os
from dotenv import load_dotenv

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))


def configure_env():
    dotenv_path = os.path.join(ROOT_DIR, '.env')
    if os.path.exists(dotenv_path):
        return load_dotenv(dotenv_path)