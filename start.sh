#!/usr/bin/env bash
echo "Run tests"
pytest -n=4 tests/test_api.py
echo "Generating report"
allure serve allure-results