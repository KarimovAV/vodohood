import os

import requests
from settings import configure_env


class ApiClient:

    configure_env()

    def __init__(self):
        self.headers = {'Content-Type': 'application/json'}
        self.cookie = ""
        self.base_url = os.environ.get("BASE_URL")

    def get_request(self, endpoint=""):
        result = requests.get(url=self.base_url + endpoint,
                              headers=self.headers,
                              cookies=self.cookie)
        return result
