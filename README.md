# Установка необходимых библиотек
```bash
- pip install -r requirements.txt
```
# Запуск тестов:
```bash
- pytest -n=4 --alluredir=./allure-results tests/test_api.py
```

# CI будет выполнять две jobs (test and cover job, reports_job) при каждом merge.
