FROM python:3.11

WORKDIR .

COPY requirements.txt .

RUN pip3 install -r requirements.txt

COPY docker_src ./docker_src

CMD ["python", "-m", "pytest", "-v", "-s", "./docker_src/tests/test_api.py"]

CMD ["allure", "serve", "allure-results"]

