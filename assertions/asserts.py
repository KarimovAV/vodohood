class Asserting:

    @staticmethod
    def check_status_code(code_response, code_for_check):
        assert code_response == code_for_check, f"Статус кода запроса {code_response} != {code_for_check}"
