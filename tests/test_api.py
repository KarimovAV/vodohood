import pytest
import allure
from api.api_client import ApiClient
from assertions.asserts import Asserting
from test_data.data import Statuses

asdasdasdasdasd
class TestApi:

    def test_base_url(self):
        with allure.step("Отправка GET запроса"):
            client = ApiClient()
            request = client.get_request()
        with allure.step("Проверка, что вернулся статус кода 200"):
            Asserting.check_status_code(request.status_code, Statuses.SUCCESS.value)

    def test_page_ships(self):
        with allure.step("Отправка GET запроса"):
            client = ApiClient()
            request = client.get_request(endpoint='ships/')
        with allure.step("Проверка, что вернулся статус кода 200"):
            Asserting.check_status_code(request.status_code, Statuses.SUCCESS.value)

    def test_page_pass(self):
        with allure.step("Отправка GET запроса"):
            client = ApiClient()
            request = client.get_request(endpoint='pass/')
        with allure.step("Проверка, что вернулся статус кода 200"):
            Asserting.check_status_code(request.status_code, Statuses.SUCCESS.value)

    def test_page_sochi(self):
        with allure.step("Отправка GET запроса"):
            client = ApiClient()
            request = client.get_request(endpoint='sochi/')
        with allure.step("Проверка, что вернулся статус кода 200"):
            Asserting.check_status_code(request.status_code, Statuses.SUCCESS.value)

    def test_page_suhum(self):
        with allure.step("Отправка GET запроса"):
            client = ApiClient()
            request = client.get_request(endpoint='suhum/')
        with allure.step("Проверка, что вернулся статус кода 200"):
            Asserting.check_status_code(request.status_code, Statuses.SUCCESS.value)